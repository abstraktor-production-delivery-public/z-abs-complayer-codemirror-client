
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class CodeMirrorPopoverBuild extends ReactComponentBase {
  constructor(props) {
    super(props);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.deepCompare(this.props.message, nextProps.message);
  }
  
  renderTitle(id) {
    return (
      <div id={id}>
        <p id="debug_popover_title_title">
          Build Error
        </p>
        <button type="button" id="debug_popover_title_close" className="close" aria-label="Close"
          onClick={(e) => {
            this.props.onClose && this.props.onClose();
          }}
        >
          ×
        </button>
      </div>
    );
  }
  
  renderMessage(message) {
    if(Array.isArray(message)) {
      const results = [];
      message.forEach((msg) => {
        results.push(this.renderMessage(msg));
      });
      return results;
    }
    else {
      return (
        <p className="debug_popover_content">
          {'\u2022 ' + message}
        </p>
      );
    }
  }
  
  render() {
    return (
      <div id="debug_popover_tooltip">
        {this.renderTitle('debug_popover_title_value')}
        <div id="debug_popover_content">
          {this.renderMessage(this.props.message)}
        </div>
      </div>
    );
  }
}
