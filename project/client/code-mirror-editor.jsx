
'use strict';

import ExtensionBreakpoint from './extensions/extension-breakpoint';
import ExtensionBuild from './extensions/extension-build';
import ExtensionDarkMode from './extensions/extension-dark-mode';
import ExtensionEmptyLine from './extensions/extension-empty-line';
import ExtensionLineNumbers from './extensions/extension-line-numbers';
import ExtensionLines from './extensions/extension-lines';
import ExtensionReadOnly from './extensions/extension-read-only';
import ExtensionScroller from './extensions/extension-scroller';
import ExtensionTabSize from './extensions/extension-tab-size';
import ExtensionTooltip from './extensions/extension-tooltip';

import CodeMirrorPopover from './code-mirror-popover';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import {EditorView, keymap, highlightSpecialChars, drawSelection, highlightActiveLine, dropCursor, rectangularSelection, crosshairCursor, highlightActiveLineGutter} from "@codemirror/view";
import {EditorState, Compartment} from "@codemirror/state"
import {defaultKeymap, history, historyKeymap} from "@codemirror/commands"
import {autocompletion, completionKeymap, closeBrackets, closeBracketsKeymap} from "@codemirror/autocomplete"
import {searchKeymap, highlightSelectionMatches} from "@codemirror/search"
import {defaultHighlightStyle, syntaxHighlighting, indentOnInput, bracketMatching, foldGutter, foldKeymap} from "@codemirror/language";
import {javascript} from "@codemirror/lang-javascript"
import {css} from "@codemirror/lang-css"
import {html} from "@codemirror/lang-html"
import {json} from "@codemirror/lang-json"
import {markdown} from "@codemirror/lang-markdown"
import React from 'react';
import { createRoot } from 'react-dom/client';


export default class CodeMirrorEditor extends ReactComponentBase {
  static DEBUGGER_RUNNING = 0;
  static DEBUGGER_BREAK = 1;
  static DEBUGGER_NOT_RUNNING = 2;
  
  constructor(props) {
    super(props);
    this.codeMirror = null;
    this.code = this.props.code;
    this.focusTrigger = true;
    this.breakpoint = null;
    this.reactDivPopupRoot = null;
    this.reactDiv = null;
    this.currentTarget = null;
    this._code_mirror = null;
    this._id = '';
    this.showTooltip = 0;
    this.queryLines = new Set();
    this.isQueryLineSet = false;
    this.queryLine = -1;
    this.extensionBreakpoint = new ExtensionBreakpoint(this.props.docKey);
    this.extensionBuild = new ExtensionBuild();
    this.extensionLinesBreakpoint = new ExtensionLines('Breakpoint', {backgroundColor: "#FFA",boxShadow:"0 0 0 1px Orange"}, {backgroundColor: "#030",boxShadow:"0 0 0 1px Orange"});
    this.lightRules = null;
    this.darkRules = null;
    this.extensionDarkMode = new ExtensionDarkMode((lightRules, darkRules) => {
      this.lightRules = lightRules;
      this.darkRules = darkRules;
    });
    this.extensionEmptyLine = new ExtensionEmptyLine();
    this.extensionLineNumbers = new ExtensionLineNumbers();
    this.extensionLinesQuery = new ExtensionLines('Query', {backgroundColor: "bisque"}, {backgroundColor: "#030"});
    this.extensionScroller = new ExtensionScroller();
    this.extensionTooltip = new ExtensionTooltip(this.lightRules, this.darkRules);
    this.updateListenerExtension = EditorView.updateListener.of((update) => {
      if(update.docChanged) {
        const doc = '\r\n' === this.props.eol ? update.state.doc.toString().replaceAll('\n', '\r\n') : update.state.doc.toString();
        this.props.onChanged && this.props.onChanged(this.props.projectId, this.props.docKey, doc);
      }
      if(update.focusChanged) {
        this.props.onFocusChange && this.props.onFocusChange(this.codeMirror.hasFocus, this.props.docKey);
      }
    });
  }
  
  didMount() {
    const options = this.props.options ? this.props.options : {};
    const extensions = [];
    const keyMaps = [
      ...closeBracketsKeymap,
      ...defaultKeymap,
      ...searchKeymap,
      ...historyKeymap,
      ...foldKeymap,
      ...completionKeymap
    ];
    extensions.push(this.updateListenerExtension);
    this.extensionLinesQuery.create(extensions, keyMaps, options, this._getQueryLines());
    this.extensionScroller.create(extensions, keyMaps, options, this._id);
    // GUTTERS
    this.extensionBreakpoint.create(extensions, keyMaps, options, this.props.name, this.props.dbugger?.breakpoints ? this.props.dbugger.breakpoints : [], !!this.props.darkMode, (lineNumber, breakpoint) => {
      this.props.onGutterChange && this.props.onGutterChange(this.props.docKey, lineNumber, breakpoint);
    });
    this.extensionLinesBreakpoint.create(extensions, keyMaps, options, []);
    this.extensionLineNumbers.create(extensions, keyMaps, options);
    if(options.foldGutter) {
      extensions.push(foldGutter());
    }
    if(options.highlightActiveLineGutter) {
	    extensions.push(highlightActiveLineGutter());
    }    
    if(options.highlightActiveLine) {
      extensions.push(highlightActiveLine());
    }
    if(options.closeBrackets) {
      extensions.push(closeBrackets());
    }
    if(options.bracketMatching) {
      extensions.push(bracketMatching());
    }
    if(options.drawSelection) {
      extensions.push(EditorState.allowMultipleSelections.of(true));
      extensions.push(rectangularSelection());
      extensions.push(crosshairCursor());
      extensions.push(drawSelection());
    }
    if(options.history) {
      extensions.push(history()); 
    }
    if(options.autocompletion) {
      extensions.push(autocompletion());
    }
    
    this.extensionBuild.create(extensions, keyMaps, options, !!this.props.darkMode);
    //extensions.push(highlightSpecialChars());
	//extensions.push(dropCursor());
   // extensions.push(indentOnInput());
    
	
	  extensions.push(highlightSelectionMatches());
    
    const themeLight = EditorView.baseTheme({
      "&.cm-editor": {
        height: "100%"
      },
      ".cm-gutters": {
        borderRight: "1px Solid #ddd !important"
      }
    });
    extensions.push(themeLight);
    this.extensionDarkMode.create(extensions, keyMaps, options, !!this.props.darkMode);
    this.extensionEmptyLine.create(extensions, keyMaps, options, !!this.props.emptyLine);
    this.extensionTooltip.create(extensions, keyMaps, options, !!this.props.darkMode, this._onDebugTooltip.bind(this));
    ExtensionReadOnly(extensions, keyMaps, options);
    ExtensionTabSize(extensions, keyMaps, options);
    
    extensions.push(keymap.of(keyMaps));
    extensions.push(syntaxHighlighting(defaultHighlightStyle, { fallback: true }));
    switch(options.type) {
      case 'js':
        extensions.push(javascript());
        break;
      case 'jsx':
        const js = javascript({jsx: true});
        extensions.push(js);
        break;
      case 'css':
        extensions.push(css());
        break;
      case 'json':
      case 'bld':
      case 'prj':
        extensions.push(json());
        break;
      case 'html':
        extensions.push(html());
        break;
      case 'md':
        extensions.push(markdown());
        break;
    };
    const state = EditorState.create({
      doc: this.props.code.replaceAll('\r\n', '\n'),
      extensions
    });
    this.codeMirror = new EditorView({
      state,
      parent: this._code_mirror
    });
    setTimeout(() => {
      const queryLines = this._getQueryLines();
      this.extensionLinesQuery.set(this.codeMirror, this.props.options, queryLines);
      if(0 !== queryLines.length) {
        this.extensionScroller.set(this.codeMirror, this.props.options, this.codeMirror.state.doc.lines, queryLines[queryLines.length - 1]);
      }
    }, 1);
  }
  
  shouldUpdate(nextProps, nextState) {
    return this.props !== nextProps;
  }
  
  didUpdate(prevProps, prevState) {
    if(prevProps.darkMode !== this.props.darkMode) {
      this.extensionDarkMode.set(this.codeMirror, this.props.options, this.props.darkMode);
    }
    if(prevProps.emptyLine !== this.props.emptyLine) {
      this.extensionEmptyLine.set(this.codeMirror, this.props.options, !!this.props.emptyLine);
    }
    if(prevProps.dbugger !== this.props.dbugger) {
      const dbugger = this.props.dbugger;
      if(prevProps.dbugger.breakpoints !== dbugger.breakpoints) {
        this.extensionBreakpoint.set(this.codeMirror, this.props.options, this.props.name, dbugger.breakpoints, this.props.darkMode);
      }
      if(CodeMirrorEditor.DEBUGGER_RUNNING === prevProps.dbugger.state) {
        if(CodeMirrorEditor.DEBUGGER_BREAK === dbugger.state) {
          if((this.props.docKey === dbugger.actor.index) && ((prevProps.dbugger.level !== dbugger.level) || (prevProps.dbugger.scripts !== dbugger.scripts))) {
            this._showDebug();
          }
          if(dbugger.level !== prevProps.dbugger.level || dbugger.objectValues !== prevProps.dbugger.objectValues) {
            this._onRenderTooltip(null);
          }
        }
        else if(CodeMirrorEditor.DEBUGGER_NOT_RUNNING === this.props.dbugger.state) {
          const code = this.props.code.replaceAll('\r\n', '\n');
          this.codeMirror.dispatch({changes: {
            from: 0,
            to: this.codeMirror.state.doc.length,
            insert: code
          }});
        }
      }
      else if(CodeMirrorEditor.DEBUGGER_BREAK === prevProps.dbugger.state) {
        if(CodeMirrorEditor.DEBUGGER_RUNNING === this.props.dbugger.state) {
          this.extensionLinesBreakpoint.set(this.codeMirror, this.props.options, []);
        }
        else if(CodeMirrorEditor.DEBUGGER_BREAK === this.props.dbugger.state) {
          if((this.props.docKey === dbugger.actor.index) && ((prevProps.dbugger.level !== dbugger.level) || (prevProps.dbugger.scripts !== dbugger.scripts))) {
            this._showDebug();
          }
          if(dbugger.level !== prevProps.dbugger.level || dbugger.objectValues !== prevProps.dbugger.objectValues) {
            this._onRenderTooltip(null);
          }
        }
      }
    }
    if('file' === this.props.currentType) {
      if(prevProps.queries !== this.props.queries) {
        const queryLines = this._getQueryLines();
        this.extensionLinesQuery.set(this.codeMirror, this.props.options, queryLines);
        if(0 !== queryLines.length) {
          this.extensionScroller.set(this.codeMirror, this.props.options, this.codeMirror.state.doc.lines, queryLines[queryLines.length - 1]);
        }
      }
    }
    if(prevProps.build !== this.props.build) {
      this.extensionBuild.set(this.codeMirror, this.props.options, this.props.build, !!this.props.darkMode);
    }
  }
  
  _showDebug() {
    const dbugger = this.props.dbugger;
    const stackLevel = dbugger.stack[dbugger.level];
    const codeLevel = dbugger.scripts.get(stackLevel.location.scriptId);
    if(codeLevel) {
      const code = codeLevel.replaceAll('\r\n', '\n');
      this.codeMirror.dispatch({changes: {
        from: 0,
        to: this.codeMirror.state.doc.length,
        insert: code
      }});
    }
    const breakpointLine = this._getBreakpointLine(stackLevel);
    if(-1 !== breakpointLine) {
      this.extensionScroller.set(this.codeMirror, this.props.options, this.codeMirror.state.doc.lines, breakpointLine);
      this.extensionLinesBreakpoint.set(this.codeMirror, this.props.options, [breakpointLine]);
    }
    this.extensionBreakpoint.set(this.codeMirror, this.props.options, stackLevel.url, dbugger.breakpoints);
  }
  
  _getBreakpointLine(stackLevel) {
    const dbugger = this.props.dbugger;
    if(dbugger.actor.index === this.props.docKey) {
      return stackLevel.location.lineNumber + 1;
    }
    else {
      return -1;
    }
  }
  
  _getQueryLines() {
    if('file' === this.props.currentType && this.props.queries) {
      const lines = this.props.queries.get(`${this.props.currentFile.path}/${this.props.currentFile.title}`);
      if(lines) {
        return lines;
      }
    }
    return [];
  }
  
  _objectData(texts) {
    for(let i = 0; i < texts.length; ++i) {
      if('this' === texts[i]) {
        const dbugger = this.props.dbugger;
        const debugData = dbugger.stack[dbugger.level];
        console.log(debugData.this);
      }
      else {
        console.log('! this');
      }
    }
  }
  
  _onRenderTooltip(texts) {
    if(null !== this.reactDivPopupRoot) {
      const dbugger = this.props.dbugger;
      const debugData = dbugger.stack[dbugger.level];
      this._objectData(texts);
      /*if(debugData.this) {
        const name = 'this_' + debugData.this.className;
        const scopesOpen = dbugger.scopesOpen;
        const open = scopesOpen.get(name).open[0];
        return  (
          <CodeMirror key="this" name="this" value={debugData.this} objectValues={dbugger.objectValues} open={open} 
            onGetMembers={(object, open) => {
              this.dispatch(TestCaseStore, new ActionTestCaseDebugGetMembers(object, open));
            }}
            onOpen={(scope, open) => {
              this.dispatch(TestCaseStore, new ActionTestCaseDebugScopeOpen(name, 0, open));
            }}
          />
        );
      }*/
      this.reactDivPopupRoot.render(<CodeMirrorPopover currentValue={dbugger.currentValue} objectValues={dbugger.objectValues} texts={texts}
        onGetMembers={(object, open) => {
          console.log('CLICK');
        }}
        onClose={() => {
          this.reactDivPopupRoot.unmount();
          this.reactDivPopupRoot = null;
          this.props.onClearCurrentValue && this.props.onClearCurrentValue();
        }}
      />);
    }
  }
  
  _onDebugTooltip(divNode, createOrDestroy, texts) {
    if(createOrDestroy) {
      this.reactDivPopupRoot = createRoot(divNode);
      this._onRenderTooltip(texts);
    }
    else {
      if(this.reactDivPopupRoot) {
        this.reactDivPopupRoot.unmount();
        this.reactDivPopupRoot = null;
      }
    }
  }
  
  /*_mouseOver(event) {
    if(1 === ++this.showTooltip) {
      this.currentTarget = event.currentTarget;
      event.currentTarget.style = 'background-color:Lavender;cursor:pointer';
      this._addPopoverProperty(event);
      this._showTooltip();
    }
  }
  
  _mouseOut(event) {
    setTimeout(() => {
      if(0 === --this.showTooltip) {
        this._removePopoverProperty();
      }
    });
  }
  
  _mouseHandler(event) {
    if(event.cancelable) {
      event.cancelBubble = true;
    }
    if("mouseover" === event.type) {
      this._mouseOver(event);
    }
    else if("mouseout" === event.type) {
      this._mouseOut(event);
    }
  }
  
  _bindPopover() {
    $("span.cm-def").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-property").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable-2").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-keyword").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
  }
  
  _unbindPopover() {
    $("span.cm-def").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-property").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable-2").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-keyword").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
  }
  
  _addPopoverProperty(event) {
    const memberNameArray = [event.currentTarget.textContent];
    let previousSibling = event.currentTarget.previousSibling;
    while (null !== previousSibling) {
      if('.' !== previousSibling.textContent) {
        break;
      }
      previousSibling = previousSibling.previousSibling;
      if(null !== previousSibling) {
        memberNameArray.unshift(previousSibling.textContent);
      }
      previousSibling = previousSibling.previousSibling;
    }
      
    this.popoverDiv = document.createElement('div');
    this.reactDiv = this.popoverDiv.appendChild(document.createElement('div'));
    this.reactDiv.setAttribute('id', 'debug_popover');
    this.reactDiv.setAttribute('style', `left:${event.clientX - 10}px;top:${event.clientY}px;`);
    CodeMirror.on(this.reactDiv, "mouseover", this._tooltipMouseHandler.bind(this));
    CodeMirror.on(this.reactDiv, "mouseout", this._tooltipMouseHandler.bind(this));
    document.body.appendChild(this.popoverDiv);
    this._getTooltipValue(memberNameArray, event.currentTarget.className);
  }
  
  _removePopoverProperty() {
    if(null !== this.popoverDiv) {
      CodeMirror.off(this.reactDiv, "mouseover", this._tooltipMouseHandler.bind(this));
      CodeMirror.off(this.reactDiv, "mouseout", this._tooltipMouseHandler.bind(this));
      this.reactDivRoot.unmount();
      this.reactDivRoot = null;
      this.reactDiv = null;
      document.body.removeChild(this.popoverDiv);
      this.popoverDiv = null;
    }
    if(null !== this.currentTarget) {
      this.currentTarget.style = '';
      this.currentTarget = null;
    }
    this.props.onClearCurrentValue && this.props.onClearCurrentValue();
  }
  
  _getTooltipValue(memberNameArray, cmType) {
    if('cm-property' === cmType && 1 <= memberNameArray.length) {
      if('this' === memberNameArray[0]) {
        this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'this');
      } 
      else {
        this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'keyword');
      }
    }
    else if('cm-keyword' === cmType) {
      if(1 === memberNameArray.length) {
        if('this' === memberNameArray[0]) {
          this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'this');
        }
        else if('let' === memberNameArray[0]) {
          this.props.onGetKeyWord && this.props.onGetKeyWord(memberNameArray); 
        }
      }
    }
    else if('cm-def' === cmType ||'cm-variable-2' === cmType) {
      this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'local');
    }
  }*/
  
  /*_showPopup(dbugger) {
    if(this.reactDiv) {
      this.reactDiv = document.getElementById('debug_popover');
    }
    if(null !== this.reactDiv) {
      if(null === this.reactDivRoot) {
        this.reactDivRoot = createRoot(this.reactDiv);
      }
      this.reactDivRoot.render(<CodeMirrorPopover currentValue={dbugger.currentValue} objectValues={dbugger.objectValues}
        onGetMembers={(object, open) => {
          this.props.onGetMembers && this.props.onGetMembers(object, open);
        }}
        onClose={() => {
          this._removePopoverProperty();
          this.showTooltip = 0;
        }}
      />);
    }
  }*/
  
  /*_tooltipMouseHandler(event) {
    if(event.cancelable) {
      event.cancelBubble = true;
    }
    if("mouseover" === event.type) {
      ++this.showTooltip;
    }
    else if("mouseout" === event.type) {
      setTimeout(() => {
        if(0 === --this.showTooltip) {
          this._removePopoverProperty();
        }
      });
    }
  }
  
  _showTooltip() {
    const dbugger = this.props.dbugger;
    this._showPopup(dbugger);
    const element = $("#debug_popover");
    if(element) {
      CodeMirror.on(element, "mouseover", this._tooltipMouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._tooltipMouseHandler.bind(this));
    }    
  }
  */
  render () {
    this._id = this.props.id ? `${this.props.id}_code_mirror_editor_textarea` : 'code_mirror_editor_textarea';
    return (
      <div className={"code_mirror_editor_view basic_same_size_as_parent"}>
	      <div className="code_mirror_editor basic_same_size_as_parent" ref={(ref) => this._code_mirror = ref} id={this._id} />
      </div>
    );
  }
}
