
'use strict';

import ThemeConst from "../themes/theme-const"
import {hoverTooltip} from "@codemirror/view"


class ExtensionTooltip {
  constructor(lightRules, darkRules) {
    this.lightRules = lightRules;
    this.darkRules = darkRules;
    this.darkMode = false;
    this.dom = null;
  }

  create(extensions, keymaps, options, darkMode, onTooltip) {
    if(!!options.debugTooltip) {
      this.darkMode = false;
      const self = this;
      const wordHover = hoverTooltip((view, pos, side) => {
        const line = view.state.doc.lineAt(pos);
        const lineView = view.docView.children[line.number - 1];
        let start = pos, end = pos;
        while(start > line.from && /\w/.test(line.text[start - line.from - 1])) {
          --start;
        }
        while(end < line.to && /\w/.test(line.text[end - line.from])) {
          ++end;
        }
        if(start == pos && side < 0 || end == pos && side > 0) {
          return null;
        }
        const startPosition = start - line.from;
        const stopPosition = end - line.from;
        const text = line.text.slice(start - line.from, end - line.from);
        let position = 0;
        //let linePart = null;
        const texts = [];
        let nextDom = null;
        let nodeData = '';
        do {
          nextDom = view.domAtPos(pos);
          //console.log(this._getType(nextDom.node.parentNode.className));
          nodeData = nextDom.node.data.trim();
          if('.' === nodeData) {
            --pos;
            continue;
          }
          texts.unshift(nodeData);
        //  console.log(nextDom.offset, nextDom.node.data);
          pos -= (nextDom.offset);
        } while(nodeData)
        texts.shift();
        console.log(texts);
        /*let prevData = '';
        for(let i = 0; i < 2500; ++i) {
          let next = view.domAtPos(i);
          if(next) {
            let nodeData = next.node.data
            if(nodeData && nodeData !== prevData) {
              nodeData = nodeData.trim();
              prevData = nodeData;
              if(nodeData) {
                const type = this._getType(next.node.parentNode.className);
                console.log(type, nodeData, ThemeConst.parsedType[type]);
              }
            }
          }
        }*/
        //console.log(texts);
        /*for(let i = 0; i < lineView.children.length; ++i) {
          linePart = lineView.children[i];
          if('MarkView' === linePart.constructor.name) {
            const textPart = linePart.children[0].text;
            position += textPart.length;
            const type = this._getType(linePart.mark.class);
            if(28 === type || 4 === type) {
              texts.push(textPart);
            }
          }
          else{
            position += lineView.children[i].text.length;
            texts = [];
          }
          if(startPosition < position) {
            break;
          }
        }*/
        return {
          pos: start,
          end,
          above: true,
          create(view) {
            self.dom = document.createElement("div");
            return {
              dom: self.dom,
              mount: (view) => {
                onTooltip(self.dom, true, texts);
              },
              destroy: (view) => {
                onTooltip(self.dom, false, []);
              }
            };
          }
        }
      });
      extensions.push(wordHover);
    }
  }
  
  set(view, options) {
    
  }
  
  _getType(className) {
    if(!this.darkMode) {
      return this.lightRules.get(className);
    }
    else {
      return this.darkRules.get(className);
    }
  }
}


module.exports = ExtensionTooltip;
