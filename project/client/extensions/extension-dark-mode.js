
'use strict';

import {vscodeDark} from '../themes/theme-dark-mode';
import {vscodeLight} from '../themes/theme-light-mode';
import {Compartment} from "@codemirror/state";
import {EditorView} from "@codemirror/view";


class ExtensionDarkMode {
  constructor(cb) {
    this.editorTheme = new Compartment();
    this.themeDark = EditorView.theme({
      "&.cm-editor": {
        height: "100%",
        scrollbarColor: "#569cff #333"
      },
      ".cm-gutters": {
        borderRight: "1px Solid #838383 !important"
      }
    });
    this.themeLight = EditorView.baseTheme({
      "&.cm-editor": {
        height: "100%"
      },
      ".cm-gutters": {
        borderRight: "1px Solid #838383 !important"
      }
    });
    // THIS IS A HACK !!!
    const lightRuleMap = this._parseClassNames(vscodeLight[1][2].value.module.rules);
    const darkRuleMap = this._parseClassNames(vscodeDark[1][2].value.module.rules);
    
  	if(cb) {
      cb(lightRuleMap, darkRuleMap);
    }
  }
  
  create(extensions, keymaps, options, darkMode) {
    if(!!options.darkMode) {
      extensions.push(this.editorTheme.of(darkMode ? [vscodeDark, this.themeDark] : [vscodeLight, this.themeLight]));
    }
  }
  
  set(view, options, darkMode) {
    if(!!options.darkMode) {
      view.dispatch({
        effects: this.editorTheme.reconfigure(darkMode ? [vscodeDark, this.themeDark] : [vscodeLight, this.themeLight])
      });
    }
  }
  
  _parseClassNames(rules) {
    const ruleMap = new Map();
    rules.forEach((rule, index) => {
      ruleMap.set(rule.substring(1, rule.indexOf(' ')), index);
    });
    return ruleMap;
  }
}


module.exports = ExtensionDarkMode;
