
'use strict';

import {EditorState, Compartment} from "@codemirror/state";
import {indentWithTab, insertTab, indentLess} from "@codemirror/commands";
import {indentUnit } from '@codemirror/language';


const ExtensionTabSize = (extensions, keymaps, options) => {
  const tabSizeCompartment = new Compartment();

  if(options.tabSize) {
    extensions.push(tabSizeCompartment.of(EditorState.tabSize.of(options.tabSize)));
  }

  if('tab' === options.indentType && !!options.tabSize && !!options.indentUnit && (options.indentUnit >= 1) && (options.tabSize >= 1) && (options.indentUnit / options.tabSize >= 1)) {
    const numberOfTabs = Math.floor(options.indentUnit / options.tabSize);
    const indentUnitString = '\t'.repeat(numberOfTabs);
    extensions.push(indentUnit.of(indentUnitString));
  }
  else if('spaces' === options.indentType && !!options.indentUnit && (options.indentUnit >= 1)) {
    const indentUnitString = ' '.repeat(options.indentUnit);
    extensions.push(indentUnit.of(indentUnitString));
  }
  else {
    console.log(`Error: wrong indentType: '${options.indentType}'`);
    return;
  }

  if('indent' === options.tabKeyAction) {
    keymaps.push(indentWithTab);
  }
  else if('tab' === options.tabKeyAction) {
    keymaps.push({
      key: 'Tab',
      preventDefault: true,
      run: insertTab
    });
    /*keymaps.push({
      key: 'Shift-Tab',
      preventDefault: true,
      run: indentLess
    });*/
  }
};


module.exports = ExtensionTabSize;
