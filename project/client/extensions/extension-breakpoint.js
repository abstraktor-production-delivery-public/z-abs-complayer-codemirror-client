
'use strict';

import {EditorView, gutter, GutterMarker} from "@codemirror/view";
import {StateField, StateEffect, RangeSet} from "@codemirror/state";


class ExtensionBreakpoint {
  constructor(docKey) {
    this.breakpointEffect = StateEffect.define({map: (val, mapping) => ({pos: mapping.mapPos(val.pos), on: val.on})});
    this.breakpointState = this._createBreakpointState();
    this.markers = [];
    this.onBreakpoint = (lineNumber, breakpoint) => {};
    this.docKey = docKey;
    this.fileName = '';
    this.breakpoints = [];
    this.darkMode = false;
  }
  
  create(extensions, keymaps, options, fileName, breakpoints, darkMode, onBreakpoint) {
    if(!!options.breakpointGutter) {
      this.fileName = fileName;
      this.breakpoints = breakpoints ? breakpoints : [];
      this.darkMode = darkMode;
      this.onBreakpoint = onBreakpoint;
      const self = this;
      extensions.push(this.breakpointState);
      extensions.push(gutter({
        class: "cm-breakpoint-gutter",
        markers: (view) => {
          return view.state.field(self.breakpointState);
        },
        lineMarker(view, line, others) {
          const lineNumber = self._lineNumber(line);
          if(lineNumber - 1 === self.markers.length) {
            const breakpoint = self._breakpoint(lineNumber - 1, self.breakpoints);
            const breakpointMarker = new BreakpointMarker(lineNumber, breakpoint, self.darkMode);
            self.markers.push({
              breakpointMarker,
              breakpoint: breakpoint
            });
          }
          else if(lineNumber - 1 > self.markers.length) {
            const start = self.markers.length;
            const stop = lineNumber - 1;
            for(let nextLineNumber = start; nextLineNumber <= stop; ++nextLineNumber) {
              const breakpoint = self._breakpoint(nextLineNumber - 1, self.breakpoints);
              const breakpointMarker = new BreakpointMarker(nextLineNumber, breakpoint, self.darkMode);
              self.markers.push({
                breakpointMarker,
                breakpoint: breakpoint
              });
            }
          }
          else {
            const breakpoint = self._breakpoint(lineNumber - 1, self.breakpoints);
            if(self.markers[lineNumber - 1].breakpoint !== breakpoint) {
              self.markers[lineNumber - 1].breakpoint = breakpoint;
              self.markers[lineNumber - 1].breakpointMarker = new BreakpointMarker(lineNumber, breakpoint, self.darkMode);
            }
          }
          return self.markers[lineNumber - 1].breakpointMarker;
        },
		    lineMarkerChange: (view) => true,
        domEventHandlers: {
          mousedown(view, line) {
            self._toggleBreakpoint(view, self._lineNumber(line));
            return true;
          }
        }
      }));
      extensions.push(EditorView.baseTheme({
        ".cm-breakpoint-gutter .cm-gutterElement": {
          paddingLeft: "2px",
          cursor: "default"
        }
      }));
    }
  }

  set(view, options, fileName , breakpoints) {
    if(!!options.breakpointGutter) {
      this.fileName = fileName;
      this.breakpoints = breakpoints ? breakpoints : [];
      view.dispatch({
        effects: this.breakpointEffect.of({})
      });
    }
  }
  
  _lineNumber(line) {
    return Math.round(1 + line.top / line.height);
  }
  
  _breakpoint(lineNumber, breakpoints) {
    const foundIndex = breakpoints.findIndex((breakpoint) => {
      return lineNumber === breakpoint.lineNumber && this.docKey === breakpoint.docKey && (this.fileName === breakpoint.name || this.fileName === breakpoint.url);
    });
    return -1 !== foundIndex;
  }
  
  _toggleBreakpoint(view, lineNumber) {
    this.onBreakpoint(lineNumber - 1, this.markers[lineNumber - 1].breakpoint);
  }
  
  _createBreakpointState() {
    const self = this;
    return StateField.define({
      create() {return RangeSet.empty},
      update(set, transaction) {
        set = set.map(transaction.changes);
        return set;
      }
    });
  }
}

class BreakpointMarker extends GutterMarker {
  constructor(lineNumber, breakpoint, darkMode) {
    super();
    this.lineNumber = lineNumber;
    this.breakpoint = breakpoint;
    this.darkMode = darkMode;
  }
  
  toDOM() {
    const classNameDebugMarker = this.breakpoint ? (this.darkMode ? 'debug_marker_breakpoint_dark' : 'debug_marker_breakpoint_light') : (this.darkMode ? 'debug_marker_dark' : 'debug_marker_light');
    const marker = document.createElement('div');
    marker.classList.add('debug_marker');
    const nbrSpan = marker.appendChild(document.createElement('span'));
    nbrSpan.classList.add('debug_marker');
    nbrSpan.classList.add(classNameDebugMarker);
    nbrSpan.appendChild(document.createTextNode(this.lineNumber));
    if(this.breakpoint) {
      const svg = marker.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'svg'));
      svg.classList.add('debug_marker');
      svg.setAttribute('width', 37);
      svg.setAttribute('height', 16);
      const group = svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
      const arrow = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
      arrow.setAttribute('points', '0,1 24,1, 30,8.5 24,16 0,16');
 //     if(pauseOnBreak) {
        arrow.setAttribute('fill', 'CornflowerBlue');
   //   }
 //     else {
 //       arrow.setAttribute('fill', 'LightBlue');
 //     }
    }
    return marker;
  }
}


module.exports = ExtensionBreakpoint;
