
'use strict';

import {EditorView, ViewPlugin, Decoration, DecorationSet, ViewUpdate} from "@codemirror/view";
import {StateEffect, StateField, RangeSet, RangeSetBuilder, Facet} from "@codemirror/state";


class ExtensionLines {
  constructor(name, lineCssLight, lineCssDark) {
    this.extensionName = `lines${name}`
    this.lineCssLight = lineCssLight;
    this.lineCssDark = lineCssDark;
    this.lines = [];
    this.lineDecoration = null;
    this.linesEffect = StateEffect.define({map: (val, mapping) => ({pos: mapping.mapPos(val.pos), on: val.on})});
    this.linesState = this._createLinesState();
    this.doUpdate = false;
  }
  
  create(extensions, keymaps, options, lines) {
    this.lines = lines;
    const extentionOption = Reflect.get(options, this.extensionName);
    if(!!extentionOption) {
      this.lineDecoration = Decoration.line({
        attributes: {class: `cm-${this.extensionName}`}
      });
      const styles = {};
      Reflect.set(styles, `&light .cm-${this.extensionName}`, this.lineCssLight);
      Reflect.set(styles, `&dark .cm-${this.extensionName}`, this.lineCssDark);
      extensions.push(this.linesState);
      extensions.push(EditorView.baseTheme(styles));
      extensions.push(this._create());
    }
  }
  
  set(view, options, lines) {
    const extentionOption = Reflect.get(options, this.extensionName);
    if(!!extentionOption) {
      this.lines = lines;
      view.dispatch({
        effects: this.linesEffect.of({})
      });
    }
  }
  
  _lineDeco(view) {
    let builder = new RangeSetBuilder();
    for(let {from, to} of view.visibleRanges) {
      for(let pos = from; pos <= to;) {
        let line = view.state.doc.lineAt(pos);
        const foundIndex = this.lines.indexOf(line.number);
        if(-1 !== foundIndex) {
          builder.add(line.from, line.from, this.lineDecoration)
        }
        pos = line.to + 1;
      }
    }
    return builder.finish();
  }
  
  _create() {
    const self = this;
    return ViewPlugin.fromClass(class {
      constructor(view) {
        this.decorations = self._lineDeco(view);
      }      
      update(update) {
        if(self.doUpdate || update.viewportChanged || update.viewportChanged) {
          self.doUpdate = false;
          this.decorations = self._lineDeco(update.view);
        }
      }
    }, {
      decorations: v => v.decorations
    });
  }

  _createLinesState() {
    const self = this;
    return StateField.define({
      create() { return RangeSet.empty },
      update(set, transaction) {
        self.doUpdate = true;
        set = set.map(transaction.changes);
        return set;
      }
    });
  }
}


module.exports = ExtensionLines;
