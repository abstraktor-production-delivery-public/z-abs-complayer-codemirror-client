
'use strict';

import {lineNumbers} from "@codemirror/view";


class ExtentionLineNumbers {
  constructor() {
    
  }

  create(extensions, keymaps, options, darkMode) {
    if(!!options.lineNumbers && !options.breakpointGutter) {
      extensions.push(lineNumbers());
    }
  }

  set(view, options, emptyLine) {
    if(!!options.lineNumbers && !options.breakpointGutter) {

    }
  }
}


module.exports = ExtentionLineNumbers;
