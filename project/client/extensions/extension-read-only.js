
'use strict';

import { EditorState } from "@codemirror/state";


const ExtensionReadOnly = (extensions, keymaps, options) => {
  extensions.push(EditorState.readOnly.of(!!options.readOnly)); 
};


module.exports = ExtensionReadOnly;
