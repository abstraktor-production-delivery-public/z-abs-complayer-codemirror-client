
'use strict';

import {EditorView, gutter, GutterMarker} from "@codemirror/view";
import {StateField, StateEffect, RangeSet} from "@codemirror/state";


class ExtensionEmptyLine {
  constructor() {
    this.emptyLine = false;
    this.emptyLineGutterMarker = new class extends GutterMarker {
      toDOM() { return document.createTextNode("ø") }
    };
    this.emptyLineEffect = StateEffect.define({});
  }

  _isEmpty(line) {
    return (this.emptyLine && line.from == line.to) ? this.emptyLineGutterMarker : null;
  }
  
  create(extensions, keymaps, options, emptyLine) {
    if(!!options.emptyLine) {
      this.emptyLine = emptyLine;
      const self = this;
      extensions.push(gutter({
        class: "cm-empty-line-gutter",
        lineMarker(view, line) {
          return self._isEmpty(line);
        },
        initialSpacer: () => this.emptyLineGutterMarker,
		    lineMarkerChange: (view) => true
      }));
      extensions.push(EditorView.baseTheme({
        ".cm-empty-line-gutter": {
          color: "#569cff !important",
          paddingRight: "2px"
        },
        ".cm-empty-line-gutter > .cm-activeLineGutter": {
          color: "#569cff !important"
        },
        ".cm-scroller": {
          overflow: "auto"
        }
      }));
    }
  }

  set(view, options, emptyLine) {
    if(!!options.emptyLine) {
      this.emptyLine = emptyLine;
      view.dispatch({
        effects: this.emptyLineEffect.of({})
      });
    }
  }
}


module.exports = ExtensionEmptyLine;
