
'use strict';

import CodeMirrorPopoverBuild from '../code-mirror-popover-build';
import {EditorView, gutter, GutterMarker} from "@codemirror/view";
import {StateField, StateEffect, RangeSet} from "@codemirror/state";
import React from 'react';
import { createRoot } from 'react-dom/client';


class ExtensionBuild {
  constructor() {
    this.buildEffect = StateEffect.define({map: (val, mapping) => ({pos: mapping.mapPos(val.pos), on: val.on})});
    this.buildState = this._createBuildState();
    this.markers = [];
    this.buildData = null;
    this.onBreakpoint = (lineNumber, breakpoint) => {};
    this.darkMode = false;
    this.changed = false;
  }
  
  create(extensions, keymaps, options, darkMode) {
    if(!!options.buildGutter) {
      this.darkMode = darkMode;
      const self = this;
      extensions.push(this.buildState);
      extensions.push(gutter({
        class: "cm-build-gutter",
        markers: (view) => {
          return view.state.field(self.buildState);
        },
        lineMarker(view, line, others) {
          const lineNumber = self._lineNumber(line);
          if(lineNumber - 1 === self.markers.length) {
            const errors = self._build(lineNumber - 1, self.buildData);
            const buildMarker = new BuildMarker(self, lineNumber, errors, self.darkMode);
            self.markers.push({
              buildMarker,
              errors: errors,
              visible: false,
              event: null
            });
          }
          else if(lineNumber - 1 > self.markers.length) {
            const start = self.markers.length;
            const stop = lineNumber - 1;
            for(let nextLineNumber = start; nextLineNumber <= stop; ++nextLineNumber) {
              const errors = self._build(nextLineNumber - 1, self.buildData);
              const buildMarker = new BuildMarker(self, nextLineNumber, errors, self.darkMode);
              self.markers.push({
                buildMarker,
                errors: errors,
                visible: false,
                event: null
              });
            }
          }
          else {
            const errors = self._build(lineNumber - 1, self.buildData);
            if(!self._isBuildEqual(self.markers[lineNumber - 1].errors, errors)) {
              self.markers[lineNumber - 1].errors = errors;
              self.markers[lineNumber - 1].buildMarker = new BuildMarker(self, lineNumber, errors, self.darkMode);
            }
          }
          return self.markers[lineNumber - 1].buildMarker;
        },
        lineMarkerChange(view) {
          const changed = this.changed;
          this.changed = false;
          return changed;
        },
        domEventHandlers: {
          mouseover(view, line, event) {
            self._mouseover(self._lineNumber(line), event);
            return true;
          }
        }
      }));
      extensions.push(EditorView.baseTheme({
        ".cm-build-gutter": {
          overflow: "visible"
        },
        ".cm-build-gutter .cm-gutterElement": {
          paddingLeft: "2px",
          cursor: "default"
        }
      }));
    }
  }
  
  set(view, options, buildData, darkMode) {
    if(!!options.buildGutter) {
      this.darkMode = darkMode;
      this.buildData = buildData;
      this.changed = true;
      view.dispatch({
        effects: this.buildEffect.of({})
      });
    }
  }
  
  _lineNumber(line) {
    return Math.round(1 + line.top / line.height);
  }
  
  _build(lineNumber, buildData) {
    let errors = [];
    if(!!buildData && buildData.result && !buildData.success) {
      buildData.errors.forEach((data) => {
        if(lineNumber === data.line - 1) {
          errors.push(data.msg);
        }
      });
    }
    return errors;
  }
  
  _isBuildEqual(previousErrors, errors) {
    if(previousErrors.length !== errors.length) {
      return false;
    }
    for(let i = 0; i < errors.length; ++i) {
      if(previousErrors[i] !== errors[i]) {
        return false;
      }
    }
    return true;
  }
  
  _popup(line, event) {
    const marker = this.markers[line - 1];
    if(!marker.visible && 0 !== marker.errors.length) {
      marker.visible = true;
      marker.event = event;
      const popoverDiv = marker.buildMarker.markerNode.appendChild(document.createElement('div'));
      popoverDiv.setAttribute('style', `position:relative;left:16px;top:-10px;z-index:5;`);
      const root = createRoot(popoverDiv);
      root.render(<CodeMirrorPopoverBuild message={marker.errors}
        onClose={() => {
          marker.visible = false;
          marker.buildMarker.markerNode.removeChild(popoverDiv);
        }}
      />);
    }
  }
  
  _mouseover(line, event) {
    this._popup(line, event);
  }
  
  _createBuildState() {
    const self = this;
    return StateField.define({
      create() {return RangeSet.empty},
      update(set, transaction) {
        set = set.map(transaction.changes);
        return set;
      }
    });
  }
}

class BuildMarker extends GutterMarker {
  constructor(extensionBuild, lineNumber, errors, darkMode) {
    super();
    this.extensionBuild = extensionBuild;
    this.lineNumber = lineNumber;
    this.errors = errors;
    this.darkMode = darkMode;
    this.markerNode = null;
  }
  
  toDOM() {
    this.markerNode = document.createElement("div");
    this.markerNode.classList.add('build_marker');
    if(0 !== this.errors.length) {
      const svg = this.markerNode.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'svg'));
      svg.classList.add('build_marker');
      svg.setAttribute('width', 10);
      svg.setAttribute('height', 10);
      const group = svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
      const circle = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
      circle.classList.add('build_marker');
      circle.setAttribute('cx', 5);
      circle.setAttribute('cy', 5);
      circle.setAttribute('r', 5);
      const line1 = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
      line1.classList.add('build_marker');
      line1.setAttribute('x1', 0);
      line1.setAttribute('x2', 10);
      line1.setAttribute('y1', 0);
      line1.setAttribute('y2', 10);
      const line2 = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
      line2.classList.add('build_marker');
      line2.setAttribute('x1', 0);
      line2.setAttribute('x2', 10);
      line2.setAttribute('y1', 10);
      line2.setAttribute('y2', 0);
      const markerData = this.extensionBuild.markers[this.lineNumber - 1];
      if(markerData.visible) {
        markerData.visible = false;
    //    setTimeout(() => {
        this.extensionBuild._popup(this.lineNumber, markerData.event)
    //      }, 1);
      }
    }
    return this.markerNode;
  }
}


module.exports = ExtensionBuild;
