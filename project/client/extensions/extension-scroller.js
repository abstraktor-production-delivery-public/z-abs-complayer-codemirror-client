
'use strict';

import {StateField, StateEffect, RangeSet} from "@codemirror/state";


class ExtensionScroller {
  constructor() {
    this.query = '';
  }
  
  create(extensions, keymaps, options, id) {
    this.query = `#${id} > .cm-editor > .cm-scroller`;
  }
  
  set(view, options, lines, lineNumber) {
    const element = document.querySelector(this.query);
    if(element) {
      const height = element.getBoundingClientRect().height;
      let scrollTop = element.scrollHeight *  (lineNumber / lines) - (height / 2.0);
      element.scrollTop = Math.max(0, Math.min(scrollTop, element.scrollHeight - height));
    }
  }
}


module.exports = ExtensionScroller;
