
'use strict';

import _extends from "@babel/runtime/helpers/extends";
import { tags as t } from '@lezer/highlight';
import { createTheme } from '@uiw/codemirror-themes';


export var defaultSettingsVscodeLight = {
  background: '#ffffff',
  foreground: '#383a42',
  caret: '#000',
  sselection: "#d9d9d9",
  selectionMatch: "#99ff7780;",
  lineHighlight: "#cceeff44",
  gutterBackground: '#f5f5f5',
  gutterForeground: '#6c6c6c',
  gutterActiveForeground: '#1e1e1e',
  fontFamily: 'Menlo, Monaco, Consolas, "Andale Mono", "Ubuntu Mono", "Courier New", monospace'
};

function get(target, prop, receiver) {
  return Reflect.get(target, prop, receiver);
}

var handler = {
  'get': get
};

export function vscodeLightInit(options) {
  const {
    theme = 'light',
    settings = {},
    styles = []
  } = options || {};
  return createTheme({
    theme: theme,
    settings: _extends({}, defaultSettingsVscodeLight, settings),
    styles: new Proxy([{ // 0
      tag: t.keyword,
      color: '#0000ff'
    },{ // 1
      tag: t.operatorKeyword,
      color: '#0000ff'
    },{ // 2
      tag: t.modifier,
      color: '#0000ff'
    },{ // 3
      tag: t.color,
      color: '#0000ff'
    },{ // 4
      tag: t.constant(t.name),
      color: '#0000ff'
    },{ // 5
      tag: t.standard(t.name),
      color: '#0000ff'
    },{ // 6
      tag: t.standard(t.tagName),
      color: '#0000ff'
    },{ // 7
      tag: t.special(t.brace),
      color: '#0000ff'
    },{  // 8
      tag: t.atom,
      color: '#0000ff'
    },{  // 9
      tag: t.bool,
      color: '#0000ff'
    },{ // 10
      tag: t.special(t.variableName),
      color: '#0000ff'
    },{ // 52
      tag: t.local(t.variableName),
      color: '#0000ff',
      backgroundColor: "#f00"
    }, { // 11
      tag: t.controlKeyword,
      color: '#af00db'
    }, { // 12
      tag: t.moduleKeyword,
      color: '#af00db'
    }, { // 13
      tag: t.name,
      color: '#0070c1'
    }, { // 14
      tag: t.deleted,
      color: '#0070c1'
    }, { // 15
      tag: t.character,
      color: '#0070c1'
    }, { // 16
      tag: t.macroName,
      color: '#0070c1'
    }, { // 17
      tag: t.propertyName,
      color: '#0070c1'
    }, { // 18
      tag: t.variableName,
      color: '#0070c1'
    }, { // 19
      tag: t.labelName,
      color: '#0070c1'
    }, { // 20
      tag: t.definition(t.name),
      color: '#0070c1'
    }, { // 21
      tag: t.heading,
      fontWeight: 'bold',
      color: '#0070c1'
    }, { // 22
      tag: t.typeName,
      color: '#267f99'
    }, { // 23
      tag: t.className,
      color: '#267f99'
    }, { // 24
      tag: t.tagName,
      color: '#267f99'
    }, { // 25
      tag: t.number,
      color: '#267f99'
    }, { // 26
      tag: t.changed,
      color: '#267f99'
    }, { // 27
      tag: t.annotation,
      color: '#267f99'
    }, { // 28
      tag: t.self,
      color: '#267f99'
    }, { // 29
      tag: t.namespace,
      color: '#267f99'
    }, { // 30
      tag: t.function(t.variableName),
      color: '#795e26'
    }, { // 31
      tag: t.function(t.propertyName),
      color: '#795e26'
    }, { // 32
      tag: t.number,
      color: '#098658'
    }, { // 33
      tag: t.operator,
      color: '#383a42'
    }, { // 34
      tag: t.punctuation,
      color: '#383a42'
    }, { // 35
      tag: t.separator,
      color: '#383a42'
    }, { // 36
      tag: t.url,
      color: '#383a42'
    }, { // 37
      tag: t.escape,
      color: '#383a42'
    }, { // 38
      tag: t.regexp,
      color: '#383a42'
    }, { // 39
      tag: t.regexp,
      color: '#af00db'
    }, { // 40
      tag: t.special(t.string),
      color: '#a31515'
    }, { // 41
      tag: t.processingInstruction,
      color: '#a31515'
    }, { // 42
      tag: t.string,
      color: '#a31515'
    }, { // 43
      tag: t.inserted,
      color: '#a31515'
    }, { // 44
      tag: t.angleBracket,
      color: '#383a42'
    }, { // 45
      tag: t.strong,
      fontWeight: 'bold'
    }, { // 46
      tag: t.emphasis,
      fontStyle: 'italic'
    }, { // 47
      tag: t.strikethrough,
      textDecoration: 'line-through'
    }, { // 48
      tag: t.meta,
      color: '#008000'
    }, { // 49
      tag: t.comment,
      color: '#008000'
    }, { // 50
      tag: t.invalid,
      color: '#e45649'
    }, { // 51
      tag: t.link,
      color: '#4078f2',
      textDecoration: 'underline'
    }, ...styles], handler)
  });
}


export var vscodeLight = vscodeLightInit();
