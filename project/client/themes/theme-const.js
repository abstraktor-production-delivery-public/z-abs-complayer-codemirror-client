
'use strict';


class ThemeConst {
  static i = -1;
  static keyword = ++ThemeConst.i;
  static operatorKeyword = ++ThemeConst.i;
  static modifier = ++ThemeConst.i;
  static color = ++ThemeConst.i;
  static constant_name = ++ThemeConst.i;
  
  static standard_name = ++ThemeConst.i;
  static standard_tagName = ++ThemeConst.i;
  static special_brace = ++ThemeConst.i;
  static atom = ++ThemeConst.i;
  static bool = ++ThemeConst.i;
  
  static special_variableName = ++ThemeConst.i;
  static local = ++ThemeConst.i;
  static controlKeyword = ++ThemeConst.i;
  static moduleKeyword = ++ThemeConst.i;
  static name = ++ThemeConst.i;
  static deleted = ++ThemeConst.i;
  
  static character = ++ThemeConst.i;
  static macroName = ++ThemeConst.i;
  static propertyName = ++ThemeConst.i;
  static variableName = ++ThemeConst.i;
  static labelName = ++ThemeConst.i;
  
  static definition_name = ++ThemeConst.i;
  static heading = ++ThemeConst.i;
  static typeName = ++ThemeConst.i;
  static className = ++ThemeConst.i;
  static tagName = ++ThemeConst.i;
  
  static number = ++ThemeConst.i;
  static changed = ++ThemeConst.i;
  static annotation = ++ThemeConst.i;
  static self = ++ThemeConst.i;
  static namespace = ++ThemeConst.i;
  
  static function_variableName = ++ThemeConst.i;
  static function_propertyName = ++ThemeConst.i;
  static number_2 = ++ThemeConst.i;
  static operator = ++ThemeConst.i;
  static punctuation = ++ThemeConst.i;
  
  static separator = ++ThemeConst.i;
  static url = ++ThemeConst.i;
  static escape = ++ThemeConst.i;
  static regexp = ++ThemeConst.i;
  static regexp = ++ThemeConst.i;
  static special_string = ++ThemeConst.i;
  
  static processingInstruction = ++ThemeConst.i;
  static string = ++ThemeConst.i;
  static inserted = ++ThemeConst.i;
  static angleBracket = ++ThemeConst.i;
  static strong = ++ThemeConst.i;

  static emphasis = ++ThemeConst.i;
  static strikethrough = ++ThemeConst.i;
  static meta = ++ThemeConst.i;
  static comment = ++ThemeConst.i;
  static link = ++ThemeConst.i;

  static invalid = ++ThemeConst.i;
  

  static parsedType = [
    'keyword',
    'operatorKeyword',
    'modifier',
    'color',
    'constant_name',
    
    'standard_name',
    'standard_tagName',
    'special_brace',
    'atom',
    'bool',
    
    'special_variableName',
    'local',
    'controlKeyword',
    'moduleKeyword',
    'name',
    'deleted',
    
    'character',
    'macroName',
    'propertyName',
    'variableName',
    'labelName',
    
    'definition_name',
    'heading',
    'typeName',
    'className',
    'tagName',
    
    'number',
    'changed',
    'annotation',
    'self',
    'namespace',
    
    'function_variableName',
    'function_propertyName',
    'number_2',
    'operator',
    'punctuation',
    
    'separator',
    'url',
    'escape',
    'regexp',
    'regexp',
    'special_string',
    
    'processingInstruction',
    'string',
    'inserted',
    'angleBracket',
    'strong',
    
    'emphasis',
    'strikethrough',
    'meta',
    'comment',
    'link',
    
    'invalid'
  ];
}


module.exports = ThemeConst;
