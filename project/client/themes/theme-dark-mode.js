
'use strict';

import _extends from "@babel/runtime/helpers/extends";
import { tags as t } from '@lezer/highlight';
import { createTheme } from '@uiw/codemirror-themes';


export var defaultSettingsVscodeDark = {
  background: '#1e1e1e',
  foreground: '#9cdcfe',
  caret: '#c6c6c6',
  selection: '#6199ff2f',
  selectionMatch: '#72a1ff59',
  lineHighlight: '#ffffff0f',
  gutterBackground: '#1e1e1e',
  gutterForeground: '#838383',
  gutterActiveForeground: '#fff',
  fontFamily: 'Menlo, Monaco, Consolas, "Andale Mono", "Ubuntu Mono", "Courier New", monospace'
};

function get(target, prop, receiver) {
  return Reflect.get(target, prop, receiver);
}

var handler = {
  'get': get
};

export function vscodeDarkInit(options) {
  const {
    theme = 'dark',
    settings = {},
    styles = []
  } = options || {};
  return createTheme({
    theme: theme,
    settings: _extends({}, defaultSettingsVscodeDark, settings),
    styles: new Proxy([{ // 0
      tag: [t.keyword],
      color: '#569cd6'
    },{ // 1
      tag: [t.operatorKeyword],
      color: '#569cd6'
    },{ // 2
      tag: [t.modifier],
      color: '#569cd6'
    },{ // 3
      tag: [t.color],
      color: '#569cd6'
    },{ // 4
      tag: [t.constant(t.name)],
      color: '#569cd6'
    },{ // 5
      tag: [t.standard(t.name)],
      color: '#569cd6'
    },{ // 6
      tag: [t.standard(t.tagName)],
      color: '#569cd6'
    },{ // 7
      tag: [t.special(t.brace)],
      color: '#569cd6'
    },{ // 8
      tag: [t.atom],
      color: '#569cd6'
    },{ // 9
      tag: [t.bool],
      color: '#569cd6'
    },{ // 10
      tag: [t.special(t.variableName)],
      color: '#569cd6'
    },{ // 52
      tag: t.local(t.variableName),
      color: '#0000ff',
      backgroundColor: "#f00"
    }, { // 11
      tag: [t.controlKeyword],
      color: '#c586c0'
    }, { // 12
      tag: [t.moduleKeyword],
      color: '#c586c0'
    }, { // 13
      tag: [t.name],
      color: '#9cdcfe'
    }, { // 14
      tag: [t.deleted],
      color: '#9cdcfe'
    }, { // 15
      tag: [t.character],
      color: '#9cdcfe'
    }, { // 16
      tag: [t.macroName],
      color: '#9cdcfe'
    }, { // 17
      tag: [t.propertyName],
      color: '#9cdcfe'
    }, { // 18
      tag: [t.variableName],
      color: '#9cdcfe'
    }, { // 19
      tag: [t.labelName],
      color: '#9cdcfe'
    }, { // 20
      tag: [t.definition(t.name)],
      color: '#9cdcfe'
    }, { // 21
      tag: t.heading,
      fontWeight: 'bold',
      color: '#9cdcfe'
    }, { // 22
      tag: [t.typeName],
      color: '#4ec9b0'
    }, { // 23
      tag: [t.className],
      color: '#4ec9b0'
    }, { // 24
      tag: [t.tagName],
      color: '#4ec9b0'
    }, { // 25
      tag: [t.number],
      color: '#4ec9b0'
    }, { // 26
      tag: [t.changed],
      color: '#4ec9b0'
    }, { // 27
      tag: [t.annotation],
      color: '#4ec9b0'
    }, { // 28
      tag: [t.self],
      color: '#569cd6'
    }, { // 29
      tag: [t.namespace],
      color: '#4ec9b0'
    }, { // 30
      tag: [t.function(t.variableName)],
      color: '#dcdcaa'
    }, { // 31
      tag: [t.function(t.propertyName)],
      color: '#dcdcaa'
    }, { // 32
      tag: [t.number],
      color: '#b5cea8'
    }, { // 33
      tag: [t.operator],
      color: '#d4d4d4'
    }, { // 34
      tag: [t.punctuation],
      color: '#d4d4d4'
    }, { // 35
      tag: [t.separator],
      color: '#d4d4d4'
    }, { // 36
      tag: [t.url],
      color: '#d4d4d4'
    }, { // 37
      tag: [t.escape],
      color: '#d4d4d4'
    }, { // 38
      tag: [t.regexp],
      color: '#d4d4d4'
    }, { // 39
      tag: [t.regexp],
      color: '#d16969'
    }, { // 40
      tag: [t.special(t.string)],
      color: '#ce9178'
    },{ // 41
      tag: [t.processingInstruction],
      color: '#ce9178'
    },{ // 42
      tag: [t.string],
      color: '#ce9178'
    },{ // 43
      tag: [t.inserted],
      color: '#ce9178'
    }, { // 44
      tag: [t.angleBracket],
      color: '#808080'
    }, { // 45
      tag: t.strong,
      fontWeight: 'bold'
    }, { // 46
      tag: t.emphasis,
      fontStyle: 'italic'
    }, { // 47
      tag: t.strikethrough,
      textDecoration: 'line-through'
    }, { // 48
      tag: [t.meta],
      color: '#6a9955'
    }, { // 49
      tag: [t.comment],
      color: '#6a9955'
    }, { // 50
      tag: t.link,
      color: '#6a9955',
      textDecoration: 'underline'
    }, { // 51
      tag: t.invalid,
      color: '#ff0000'
    }, ...styles], handler)
  });
}


export var vscodeDark = vscodeDarkInit();
