
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class CodeMirrorDebugVariable extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props, nextProps)
  }
  
  renderScopeValueMembersProxy(renderedMembers, name, extraMembers, depth) {
    if(undefined !== extraMembers) {
      for(let i = 0; i < extraMembers.length; ++i) {
        renderedMembers.push(this.renderScopeValue(extraMembers[i], i, depth));
      }
    }
  }
  
  renderScopeValueMembersInnerExtra(renderedMembers, extraMembers, depth) {
    if(undefined !== extraMembers) {
      const lengthObject = extraMembers.find((member) => {
        return 'length' === member.name;
      });
      if(lengthObject) {
        const length = lengthObject.value.value;
        for(let i = 0; i < length; ++i) {
          const member = extraMembers.find((member) => {
            return i.toString() === member.name;
          });
          renderedMembers.push(this.renderScopeValue(member, i, depth));
        }
      }
    } 
  }
  
  renderScopeValueMembersInner(renderedMembers, members, depth) {
    members.forEach((member, index) => {
      renderedMembers.push(this.renderScopeValue(member, index, depth));
    });
  }
  
  renderScopeValueMembers(object, depth, div) {
    if(null !== object.value) {
      const objectValue = this.props.objectValues.get(object.objectId);
      const renderedMembers = [];
      let divClass = 'debug_scope_closed_object';
      if(undefined !== objectValue && objectValue.open) {
        if('map' === object.subtype) {
          this.renderScopeValueMembersInnerExtra(renderedMembers, objectValue.extraMembers, depth);
          this.renderScopeValueMembersInner(renderedMembers, objectValue.members, depth);
          divClass = 'debug_scope_open_object';
        }
        else if('proxy' === object.subtype) {
          this.renderScopeValueMembersProxy(renderedMembers, object.decription, objectValue.extraMembers, depth);
          //this.renderScopeValueMembersInner(renderedMembers, objectValue.members, depth);
          divClass = 'debug_scope_open_object';
        }
        else {
          this.renderScopeValueMembersInnerExtra(renderedMembers, objectValue.extraMembers, depth);
          this.renderScopeValueMembersInner(renderedMembers, objectValue.members, depth);
          divClass = 'debug_scope_open_object';
        }
      }
      return (
        <div ref={(svg) => { div.push(svg); }} className={divClass}>
          <ul className="nav nav-pills nav-stacked debug_scope">
            {renderedMembers}
          </ul>
        </div>
      );
    }
  }
  
  renderScopeImage(object, isNull) {
    if(isNull) {
      return null;
    }
    let svgClass = 'debug_scope_closed_object';
    if('object' === object.type) {
      const objectValue = this.props.objectValues.get(object.objectId);
      if(undefined !== objectValue && objectValue.open) {
        svgClass = 'debug_scope_open_object';
      }
    }
    else {
      if(this.props.open) {
        svgClass = 'debug_scope_open_object';
      }
    }
    const ref = React.createRef();
    return (
      <svg ref={ref} className={svgClass} width="9" height="9"
        onClick={(e) => {
          e.preventDefault();
          const open = !!ref.current?.classList.contains('debug_scope_open_object');
          if('object' === object.type) {
            this.props.onGetMembers && this.props.onGetMembers(object, !open);
          }
          else {
            this.props.onOpen && this.props.onOpen(object.type, !open);
          }
        }
      }>
        <g>
          <polygon points="1,1 1,7 7,4" stroke="Grey" strokeWidth="1" fill="white" />
        </g>
      </svg>
    );
  }

  renderScopeValueUndefined() {
    return (
      <span className="debug_def debug_scope">
        undefined
      </span>
    );
  }
  
  renderScopeValueNull() {
    return (
      <span className="debug_atom debug_scope">
        null
      </span>
    );
  }
  
  renderScopeValueObject(object, isNull, isUndefined) {
    if(isUndefined) {
      return this.renderScopeValueUndefined();
    }
    else if(isNull) {
      return this.renderScopeValueNull();
    }
    else {
      return (
        <span className="debug_variable debug_scope">
          {object.value.className}
        </span>
      );
    }
  }
    
  renderScopeValue(object, index, depth) {
    const key = index + '-' + depth;
    if(undefined !== object.value) {
      const objectValueType = object.value.type;
      if('this' === object.name) {
        console.log('****************************************************');
      }
      if(0 === depth && 'this' !== object.name && !this.props.popup) {
        const scopeValues = [];
        object.value.object.forEach((objectData, index) => {
          const scopeValue = this.renderScopeValue(objectData, index, depth + 1);
          if(scopeValue) {
            scopeValues.push(scopeValue);
          }
        });
        if(0 === scopeValues.length) {
          return null;
        }
        const divClass = this.props.open ? 'debug_scope_open_object' : 'debug_scope_closed_object';
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
              }}
            >
              {this.renderScopeImage(object.value, 0 === scopeValues.length)}
              <p className="debug_scope">
                {object.name}
              </p>
            </a>
            <div className={divClass}>
              <ul className="nav nav-pills nav-stacked debug_scope">
                {scopeValues}
              </ul>
            </div>
          </li>
        );
      }
      else if('string' === objectValueType) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
              }}
            >
              <p className="debug_scope">
                {object.name}: 
              </p>
              <span className="debug_string debug_scope">
                "{object.value.value}"
              </span>
              <p className="debug_scope_type">
                [string]
              </p>
            </a>
          </li>
        );
      }
      else if('number' === objectValueType) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              <p className="debug_scope">
                {object.name}: 
              </p>
              <span className="debug_number debug_scope">
                {object.value.value}
              </span>
              <p className="debug_scope_type">
                [number]
              </p>
            </a>
          </li>
        );
      }
      else if('boolean' === objectValueType) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              <p className="debug_scope">
                {object.name}: 
              </p>
              <span className="debug_atom debug_scope">
                {object.value.value ? 'true' : 'false'}
              </span>
              <p className="debug_scope_type">
                [boolean]
              </p>
            </a>
          </li>
        );
      }
      else if('object' === objectValueType) {
        let type = '[object]';
        const isUndefined = undefined === object.value;
        const isNull = !isUndefined && null === object.value.value;
        if(!isUndefined && undefined !== object.value.subtype) {
          if('generator' === object.value.subtype) {
            type = `[${object.value.subtype}]`;
          }
          else if(!isNull) {
            type = `[${object.value.description}]`;
          }
        }
        const divArray = [];
        const members = this.renderScopeValueMembers(object.value, depth + 1, divArray);
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              {this.renderScopeImage(object.value, isNull)}
              <p className="debug_scope">
                {object.name}: 
              </p>
              {this.renderScopeValueObject(object, isNull, isUndefined)}
              <p className="debug_scope_type">
                {type}
              </p>
            </a>
            {members}
          </li>
        );
      }
      else if('undefined' === objectValueType) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              <p className="debug_scope">
                {object.name}: 
              </p>
              {this.renderScopeValueUndefined()}
            </a>
          </li>
        );
      }
      else {
        return null;
      }
    }
  }

  render() {
    return (
      <ul className="nav nav-pills nav-stacked debug_scope">
        {
          this.renderScopeValue({
            name: this.props.name,
            value: this.props.value
          }, 0, 0)
        }
      </ul>
    );
  }
}
