
'use strict';

import CodeMirrorDebugVariable from './code-mirror-debug-variable';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class CodeMirrorPopover extends ReactComponentBase {
  constructor(props) {
    super(props);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.deepCompare(this.props.currentThis, nextProps.currentThis)
      || !this.deepCompare(this.props.objectValues, nextProps.objectValues);
  }
  
  renderTitle(id) {
    return (
      <div id={id}>
        <p id="debug_popover_title_title">
          {'HELLO'/*this.props.currentValue.name*/}
        </p>
        <button type="button" id="debug_popover_title_close" className="close" aria-label="Close"
          onClick={(e) => {
            this.props.onClose && this.props.onClose();
          }}
        >
          ×
        </button>
      </div>
    );
  }
  
  renderValueA(name, valueObject) {
    return (
      <div id="debug_popover_tooltip">
        <div id="debug_popover_content">
          <CodeMirrorDebugVariable popup={true} name={name} value={valueObject} objectValues={this.props.objectValues} 
            onGetMembers={(object, open) => {
              this.props.onGetMembers && this.props.onGetMembers(object, open);
            }}  
          />
        </div>
      </div>
    );
  }
  
  renderFunction() {
    return (
      <div id="debug_popover_tooltip">
        {this.renderTitle('debug_popover_title_function')}
      </div>
    );
  }
  
  renderValue() {
    console.log('renderValue:', this.props.debugData);
    if(this.props.currentValue) {
      if(0 !== this.props.texts.length) {
        if('this' === this.props.texts[0]) {
          console.log('THIS');
          return this.renderValueA(this.props.currentValue.name, this.props.currentValue);
        }
      }
      console.log('NULL');
      return null;
      /*switch(this.props.currentValue.value.type) {
        case 'function':
          return this.renderFunction();
        default:
          return this.renderValue();
      }*/
    }
    else {
      console.log('! THIS');
      return null;
    }
  }
  
  render() {
    console.log('currentValue:', this.props.currentValue);
    if(this.props.currentValue) {
      return (
        <>
          {this.renderTitle('debug_popover_title_value')}
          {this.renderValue()}
        </>
      );
    }
  }
}
